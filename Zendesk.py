from zenpy import Zenpy
from zenpy.lib.api_objects import Ticket, User, Comment


class Zendesk:
    @staticmethod
    def create_ticket(subject,description):
        zenpy_client.tickets.create(Ticket(subject=subject, description=description))

    @staticmethod
    def create_ticket_with_user(subject,description,user_name,email):
        zenpy_client.tickets.create(
            Ticket(description=description, subject=subject,
                   requester=User(name=user_name, email=email))
        )

    @staticmethod
    def search_ticket(type,status):
        return  zenpy_client.search(type=type, status=status) #eg:type = ticket status = open

    @staticmethod
    def search_ticket_userdefined_assignee(type,status,assignee_email,sort_order='desc'):
        return zenpy_client.search(type=type, status_less_than=status, assignee=assignee_email, sort_order=sort_order)

    @staticmethod
    def update_ticket_tag(ticket_id,tag:set):
        ticket = zenpy_client.tickets(id=ticket_id)
        ticket.tags.extend(tag)
        zenpy_client.tickets.update(ticket)

    @staticmethod
    def upload_attachment(ticket_id,doc_path,comment):
        upload_instance = zenpy_client.attachments.upload(doc_path)
        ticket = zenpy_client.tickets(id=ticket_id)
        ticket.comment = Comment(body=comment, uploads=[upload_instance.token])
        zenpy_client.tickets.update(ticket)


credentials = {
        "subdomain": "metricdusthelp",
        "email": "email_id",
        "token": "hmEGNpongb5vXUA9PkMYlc8VX73ACSSsXRHPWgBb",
    }
zenpy_client = Zenpy(**credentials)
# zenpy_client.tickets.create(Ticket(subject="Important", description="Thing"))
# for ticket in zenpy_client.search(type='ticket', status='open'):
#     print(ticket.requester.name)
#     print(ticket.to_dict())
#     print(ticket.to_json())

# Zendesk.create_ticket("test subject", "test description")
# Zendesk.create_ticket_with_user("subject","description","username","email")
# Zendesk.search_ticket_userdefined_assignee('ticket','open','email','desc')
# Zendesk.search_ticket('ticket','new')
# Zendesk.update_ticket_tag('ticket_id','tags of type set')
# Zendesk.upload_attachment('ticket_id','path to file','comment')
#
#

